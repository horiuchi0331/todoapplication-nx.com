import firebase from '@/plugins/firebase'

export const state = () => ({
  items: [{
    content: '[タスク名]\nタスク内容',
    toggle: false // trueで完了
  }],
})

export const getters = {
  getList (state) {
    return state.items
  }
}

export const mutations = {
  setTaskList(state, list) {
    state.items = list
  }
}

export const actions = {
  init({ commit, rootState }) {
    let todoRef = firebase.database().ref('todo/task');

    // // Firebaseからデータ取得（task）
    todoRef.on('value', function(snapshot) {
      let getData = snapshot.val();
      const items = []

      // データがない場合はdata()初期値をFirebase側にも設定
      if(getData !== null) {
        items = getData;
      } else {
        // todoRef.set(state.items);
      }
    });
    commit('todo/setTaskList',items,{ root: true })
  },
  clearItems(state) {
    state.items = state.items.filter(function(item, index) {
      return  item.toggle == false
    })
  }
}
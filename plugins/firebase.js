import firebase from "firebase/app"
import "firebase/database"

if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyD8dgoEjX2ywuTvIqEASECudeRLUTpRqvo",
    authDomain: "todo-f705a.firebaseapp.com",
    databaseURL: "https://todo-f705a.firebaseio.com",
    projectId: "todo-f705a",
    storageBucket: "todo-f705a.appspot.com",
    messagingSenderId: "522353658799",
    appId: "1:522353658799:web:e8929a6bf76a91b86a7d5a",
    measurementId: "G-53R6P082XL"
  })
}

export default firebase
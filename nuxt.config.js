export default {
  buildModules: ['@nuxt/typescript-build'],
  head: {
    htmlAttrs:{
      lang: 'ja'
    },
    title: 'todos',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'todoリストです' }
    ],
    link: [
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400&display=swap'
      },
      {
        rel: 'stylesheet',
        href: 'https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css'
      }
    ]
  },
  css: [
    { src: '@/assets/scss/style.scss', lang: 'scss' },
  ],
  plugins: [
    { src: '@/plugins/js/main.js', mode: 'client' }
  ]
}
